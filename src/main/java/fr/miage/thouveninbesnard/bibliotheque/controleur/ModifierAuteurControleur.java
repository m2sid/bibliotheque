/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.AuteurService;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 *
 * @author Nicolas
 */
public class ModifierAuteurControleur extends ModalControleur implements Initializable{
    
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;

    private Auteur auteur;
    
    private static final AuteurService auteurService = ServiceFactory.getAuteurService();
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {   
    }
    
    @FXML
    private void pressEnregistrer(ActionEvent event) throws IOException{
        okCliked = true;
        Auteur auteurTemp = new Auteur(nom.getText(), prenom.getText());
        if(!auteur.equals(auteurTemp)){
            this.auteur.updateModele(auteurTemp);
        }
        AuteurService auteurService = ServiceFactory.getAuteurService();
        auteurService.update(auteur);
        closeModal(event);
    }
    
    @FXML
    private void pressAnnuler(ActionEvent event) throws IOException{
        closeModal(event);
    }
    
    @FXML
    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
        nom.setText(this.auteur.getNom());
        prenom.setText(this.auteur.getPrenom());
    }
}
