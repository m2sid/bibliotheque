/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.MagazineDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class MagazineService implements Service<Magazine> {

    public static MagazineDao magazineDao;

    private static final MagazineService INSTANCE = new MagazineService();

    private MagazineService() {
        magazineDao = HibernateDaoFactory.getMagazineDao();
    }

    @Override
    public void create(Magazine magazine) {
        magazineDao.saveOrUpdate(magazine);
    }

    @Override
    public void update(Magazine magazine) {
        magazineDao.saveOrUpdate(magazine);
    }

    @Override
    public void delete(Magazine magazine) {
        magazineDao.delete(magazine);
    }

    @Override
    public List<Magazine> findAll() {
        return magazineDao.findAll();
    }

    @Override
    public int size() {
        return magazineDao.size();
    }

    public static MagazineService getInstance() {
        return MagazineService.INSTANCE;
    }

    public List<Magazine> findAllLike(String pattern) {
        return magazineDao.findAllLike(pattern);
    }

    public boolean exist(String titre, Integer numero) {
        return magazineDao.exist(titre, numero);
    }
}
