/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import fr.miage.thouveninbesnard.bibliotheque.util.DateUtil;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Nicolas
 */
@Entity
@Table(name = "RESERVATION")
public class Reservation implements Serializable {

    private IntegerProperty id = new SimpleIntegerProperty();

    private ObjectProperty<Date> date = new SimpleObjectProperty();

    private Oeuvre oeuvre;

    private Usager usager;

    public Reservation() {
        this.date.setValue(new Date());
    }
    
    public Reservation(Usager usager, Oeuvre oeuvre){
        this.date.setValue(new Date());
        this.oeuvre = oeuvre;
        this.usager = usager;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return this.id.getValue();
    }

    public void setId(Integer id) {
        this.id.setValue(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "date")
    public Date getDate() {
        return date.getValue();
    }

    public void setDate(Date date) {
        this.date.setValue(date);
    }

    public ObjectProperty<LocalDate> dateProperty() {
        return new SimpleObjectProperty<>(DateUtil.asLocalDate(date.getValue()));
    }

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    @JoinColumn(name = "oeuvre_id")
    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    @JoinColumn(name = "usager_id")
    public Usager getUsager() {
        return usager;
    }

    public void setUsager(Usager usager) {
        this.usager = usager;
    }
}
