/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class MenuBarControleur implements Initializable {
    @FXML
    private MenuBar menu;
    @FXML
    private Menu usager;
    @FXML
    private MenuItem ajouterUsager;
    @FXML
    private MenuItem listerUsager;
    @FXML
    private Menu oeuvre;
    @FXML
    private Menu ajouterOeuvre;
    @FXML
    private MenuItem ajouterLivre;
    @FXML
    private MenuItem ajouterMagazine;
    @FXML
    private MenuItem listerOeuvre;
    @FXML
    private Menu auteur;
    @FXML
    private MenuItem ajouterAuteur;
    @FXML
    private MenuItem listerAuteur;
    @FXML
    private Menu reservation;
    @FXML
    private MenuItem reserveOeuvre;
    @FXML
    private Menu emprunt;
    @FXML
    private MenuItem AjouterExemplaire;
    @FXML
    private MenuItem emprunterExemplaire;
    @FXML
    private MenuItem rendreExemplaire;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

    @FXML
    private void pressAjouterUsager(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterUsager.fxml");
    }

    @FXML
    private void pressListerUsager(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/ListerUsager.fxml");
    }

    @FXML
    private void pressAjouterLivre(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterLivre.fxml");
    }

    @FXML
    private void pressAjouterMagazine(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterMagazine.fxml");
    }

    @FXML
    private void pressListerOeuvre(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/ListerOeuvre.fxml");
    }

    @FXML
    private void pressAjouterAuteur(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterAuteur.fxml");
    }

    @FXML
    private void pressListerAuteur(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/ListerAuteur.fxml");
    }

    @FXML
    private void pressReserverOeuvre(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/ReserverOeuvre.fxml");
    }

    @FXML
    private void pressAjouterExemplaire(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterExemplaire.fxml");
    }
    
    @FXML
    private void pressEmprunterExemplaire(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/EmprunterOeuvre.fxml");
    }

    @FXML
    private void pressRendreExemplaire(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/RendreExemplaire.fxml");
    }
    
}
