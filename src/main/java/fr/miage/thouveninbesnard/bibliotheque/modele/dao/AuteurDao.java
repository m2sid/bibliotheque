/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class AuteurDao extends AbstractDao<Auteur> {
    
    private static final AuteurDao INSTANCE = new AuteurDao();
    
    private AuteurDao(){
        super();
    }
    
    public void create(Auteur auteur){
        super.saveOrUpdate(auteur);
    }
    
    public void delete(Auteur auteur){
        super.delete(auteur);
    }
    
    public List<Auteur> findAll(){
        return super.findAll(Auteur.class);
    }

    public int size() {
       return super.size(Auteur.class);
    }
    
    public boolean exist(Auteur auteur){
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("nom", auteur.getNom()));
        criteres.add(Restrictions.eq("prenom", auteur.getPrenom()));
        return size(Auteur.class, criteres) > 0;
    }
    
    public static AuteurDao getInstance(){
        return AuteurDao.INSTANCE;
    }
}
