/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class OeuvreDao extends AbstractDao<Oeuvre>{
    
    private static final OeuvreDao INSTANCE = new OeuvreDao();
    
    private OeuvreDao(){}
    
    public void create(Oeuvre oeuvre){
        super.saveOrUpdate(oeuvre);
    }
    
    public void delete(Oeuvre oeuvre){
        super.delete(oeuvre);
    }
    
    public List<Oeuvre> findAll(){
        super.findAll(Oeuvre.class);
        return super.findAll(Oeuvre.class);
    }

    public int size() {
       return super.size(Oeuvre.class);
    }
    
    public static OeuvreDao getInstance(){
        return OeuvreDao.INSTANCE;
    }
}
