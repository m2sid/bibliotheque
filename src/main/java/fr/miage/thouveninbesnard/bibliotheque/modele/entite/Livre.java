/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author Nicolas
 */

@Entity
@Table(name = "LIVRE")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Livre extends Oeuvre implements Serializable{
   
    private SimpleStringProperty resume = new SimpleStringProperty();
    
    private Auteur auteur;
    
    public Livre() {}
    
    public Livre(String titre, String resume, Auteur auteur){
        super(titre);
        this.resume.setValue(resume);
        this.auteur = auteur;
    }
    
    @Column(name = "resume")
    public String getResume() {
        return resume.getValue();
    }
    
    public void setResume(String resume){
        this.resume.setValue(resume);
    }
    
    public StringProperty resumeProperty() {
        return resume;
    }

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    @JoinColumn(name="auteur_id")
    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }
    
    @Override
    public StringProperty infosOeuvre(){
        return new SimpleStringProperty(titre.getValue());
    }
}
