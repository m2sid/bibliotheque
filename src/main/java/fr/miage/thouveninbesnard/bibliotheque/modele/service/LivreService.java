/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.LivreDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class LivreService implements Service<Livre> {
     
    public static LivreDao livreDao;
    
    private static final LivreService INSTANCE = new LivreService();
    
    private LivreService(){
        livreDao = HibernateDaoFactory.getLivreDao();
    }
    
    @Override
    public void create(Livre livre){
        livreDao.saveOrUpdate(livre);
    }
    
    @Override
    public void update(Livre livre) {
        livreDao.saveOrUpdate(livre);
    }
    
    @Override
    public void delete(Livre livre){
        livreDao.delete(livre);
    }
    
    @Override
    public List<Livre> findAll(){
        return livreDao.findAll();
    }

    @Override
    public int size() {
     return livreDao.size();
    }
    
    public static LivreService getInstance(){
        return LivreService.INSTANCE;
    }
    
    public List<Livre> findAllLike(String pattern){
        return livreDao.findAllLike(pattern);
    }
    
    public List<Livre> findAllByAuteur(Auteur auteur){
        return livreDao.findAllByAuteur(auteur);
    }
    
    public boolean exist(Livre livre){
        return livreDao.exist(livre);
    }
}
