/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import java.io.Serializable;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author Nicolas
 */

@Entity
@Table(name = "MAGAZINE")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Magazine extends Oeuvre  implements Serializable{
    
    private SimpleIntegerProperty numero = new SimpleIntegerProperty();   

    public Magazine() {}
    
    public Magazine(String titre, Integer numero){
        super(titre);
        this.numero.setValue(numero);
    }

    @Column(name = "numero")
    public Integer getNumero() {
        return this.numero.getValue();
    }
    
    public void setNumero(Integer numero){
        this.numero.setValue(numero);
    }
    
    public StringProperty numeroProperty() {
        return new SimpleStringProperty(String.valueOf(numero.getValue()));
    }
    
    @Override
    public StringProperty infosOeuvre(){
        return new SimpleStringProperty(titre.getValue()+" - "+this.numero.getValue());
    }
    
}
