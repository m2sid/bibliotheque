/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.EmpruntService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.UsagerService;
import fr.miage.thouveninbesnard.bibliotheque.util.DateUtil;
import java.io.IOException;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class ListerUsagerControleur implements Initializable {

    @FXML
    private TableView<Usager> tableUsager;
    @FXML
    private TableColumn<Usager, String> colonneNomUsager;
    @FXML
    private TableColumn<Usager, String> colonnePrenomUsager;
    @FXML
    private Label nom;
    @FXML
    private Label prenom;
    @FXML
    private Label dateNaissance;
    @FXML
    private Label adresse;
    @FXML
    private Label mail;
    @FXML
    private Label telephone;
    @FXML
    private Button modifier;
    @FXML
    private Button supprimer;
    @FXML
    private Button reservation;
    @FXML
    private Button buttonEmprunt;

    private static final UsagerService usagerService = ServiceFactory.getUsagerService();
    ;
    
    private static final EmpruntService empruntService = ServiceFactory.getEmpruntService();

    private ObservableList<Usager> listeUsagers;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colonneNomUsager.setCellValueFactory(cellData -> cellData.getValue().nomProperty());
        colonnePrenomUsager.setCellValueFactory(cellData -> cellData.getValue().prenomProperty());

        tableUsager.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherUsager(courant));

        listeUsagers = FXCollections.observableList((List<Usager>) usagerService.findAll());
        tableUsager.setItems(listeUsagers);
        modifier.setDisable(true);
        reservation.setDisable(true);
    }

    private void afficherUsager(Usager courant) {
        String nomUsager = courant.getNom();
        String prenomUsager = courant.getPrenom();
        Date dateNaissanceUsager = courant.getDateNaissance();
        String adresseUsager = courant.getAdresse();
        String mailUsager = courant.getMail();
        String telephoneUsager = courant.getTelephone();

        nom.setText((nomUsager != null) ? nomUsager : "");
        prenom.setText((prenomUsager != null) ? prenomUsager : "");
        dateNaissance.setText((dateNaissanceUsager != null) ? DateUtil.format(dateNaissanceUsager) : "");
        adresse.setText((adresseUsager != null) ? adresseUsager : " ");
        mail.setText((mailUsager != null) ? mailUsager : "");
        telephone.setText((telephoneUsager != null) ? telephoneUsager : "");

        modifier.setDisable(false);
        reservation.setDisable(false);
        buttonEmprunt.setDisable(false);
    }

    @FXML
    private void pressSupprimer(Event event) {
        Usager usagerCourant = tableUsager.getSelectionModel().getSelectedItem();

        List<Emprunt> listeEmprunts = empruntService.findAllByUsager(usagerCourant);

        Alert alert;
        if (listeEmprunts.isEmpty()) {
            String nomUsager = usagerCourant.getNom();
            String prenomUsager = usagerCourant.getPrenom();

            alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Confirmer la suppression de " + nomUsager + " " + prenomUsager);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                usagerService.delete(usagerCourant);
                tableUsager.getItems().remove(usagerCourant);
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(null);
                alert.setHeaderText(null);
                alert.setContentText(usagerCourant.getPrenom() + " " + usagerCourant.getNom() + " supprimé");
                alert.show();
            }
        } else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur suppression usager");
            alert.setHeaderText(null);
            String message = "L'usager n'a pas rendu tous ses exemplaires";
            for (Emprunt emprunt : listeEmprunts) {
                message = message + "\n - " + emprunt.getExemplaire().getOeuvre().infosOeuvre().getValue();
            }
            alert.setContentText(message + "\n \n");
            alert.show();
        }

    }

    @FXML
    private void pressModifier(Event event) throws IOException {
        int usagerIndex = tableUsager.getSelectionModel().getSelectedIndex();
        Usager usager = listeUsagers.get(usagerIndex);
        BibliothequeApp.getInstance().ouvrirModal("/fxml/ModifierUsager.fxml", (loader) -> {
            ModifierUsagerControleur controleur = loader.getController();
            controleur.setUsager(usager);
            return controleur;
        });
        tableUsager.getColumns().get(0).setVisible(false);
        tableUsager.getColumns().get(0).setVisible(true);
        afficherUsager(usager);
    }

    @FXML
    private void pressAfficherReservations(Event event) throws IOException {
        Usager usager = tableUsager.getSelectionModel().getSelectedItem();
        BibliothequeApp.getInstance().ouvrirModal("/fxml/ReservationsUsager.fxml", (loader) -> {
            ReservationsUsagerControleur controleur = loader.getController();
            controleur.setUsager(usager);
            return controleur;
        });
    }

    @FXML
    private void pressEmprunt(ActionEvent event) throws IOException{
        Usager usager = tableUsager.getSelectionModel().getSelectedItem();
        BibliothequeApp.getInstance().ouvrirModal("/fxml/EmpruntsUsager.fxml", (loader) -> {
            EmpruntsUsagerControleur controleur = loader.getController();
            controleur.setUsager(usager);
            return controleur;
        });
    }
}
