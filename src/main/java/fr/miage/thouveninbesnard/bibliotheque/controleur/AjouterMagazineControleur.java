/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.MagazineService;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author Nicolas
 */
public class AjouterMagazineControleur implements Initializable {

    @FXML
    private TextField titre;

    @FXML
    private TextField numero;

    private static MagazineService magazineService = ServiceFactory.getMagazineService();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void pressAjouter() throws IOException {
        String titreStr = titre.getText();
        String numeroStr = numero.getText();
        if (checkData(titreStr, numeroStr)) {
            try {
                int numeroIntger = Integer.parseInt(numero.getText());
                Magazine magazine = new Magazine(titreStr, numeroIntger);
                magazineService.create(magazine);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(null);
                alert.setHeaderText(null);
                alert.setContentText(titreStr + " - " + numeroIntger + " créé");
                alert.showAndWait();
                BibliothequeApp.getInstance().changerScene("/fxml/ListerOeuvre.fxml");
            } catch (NumberFormatException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle(null);
                alert.setHeaderText(null);
                alert.setContentText("Saisir un numéro entier");
                alert.showAndWait();
            }
        }
    }

    private boolean checkData(String titre, String numero) {
        return checkTitre(titre) && checkNumero(numero) & checkExist(titre, numero);
    }

    private boolean checkTitre(String titre) {
        if (titre.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Aucun titre spécifié");
            alert.showAndWait();
            return false;
        }
        return true;
    }

    private boolean checkNumero(String numero) {
        if (numero.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Aucun numero spécifié");
            alert.showAndWait();
            return false;
        }
        return true;
    }

    private boolean checkExist(String titre, String numero) {
        int numeroIntger = Integer.parseInt((numero));
        if (magazineService.exist(titre, numeroIntger)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(titre + " " + numero + " existe déjà");
            alert.showAndWait();
            return false;
        }
        return true;
    }
}
