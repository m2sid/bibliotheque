/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class ReservationDao extends AbstractDao<Reservation> {

    private static final ReservationDao INSTANCE = new ReservationDao();

    private ReservationDao() {
        super();
    }

    public void create(Reservation reservation) {
        super.saveOrUpdate(reservation);
    }

    public void delete(Reservation reservation) {
        super.delete(reservation);
    }

    public List<Reservation> findAll() {
        return super.findAll(Reservation.class);
    }

    public int size() {
        return super.size(Reservation.class);
    }

    public List<Reservation> findAllByUsager(Usager usager) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("usager", usager));
        return super.findAll(Reservation.class, criteres);
    }

    public List<Reservation> findAllByOeuvre(Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        return super.findAll(Reservation.class, criteres);
    }

    public int getNbResaOfOeuvre(Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        return super.size(Reservation.class, criteres);
    }

    public boolean usagerHasReserveOeuvre(Usager usager, Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        criteres.add(Restrictions.eq("usager", usager));
        return (0 < super.size(Reservation.class, criteres));
    }

    public Reservation findOneBy(Usager usager, Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        criteres.add(Restrictions.eq("usager", usager));
        return super.findOne(Reservation.class, criteres);
    }

    public boolean exist(Reservation reservation) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", reservation.getOeuvre()));
        criteres.add(Restrictions.eq("usager", reservation.getUsager()));
        return size(Reservation.class, criteres) > 0;
    }

    public static ReservationDao getInstance() {
        return ReservationDao.INSTANCE;
    }
}
