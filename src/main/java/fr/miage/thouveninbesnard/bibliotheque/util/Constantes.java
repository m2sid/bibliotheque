/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.util;

/**
 *
 * @author Nicolas
 */
public class Constantes {
    
    public static final int DUREE_EMPRUNT = + 3 * 7;
    
    public static final int ETAT_BON = 1;
    
    public static final int ETAT_ABIME = 0;
    
    public static final int STATUT_NON_EMPRUNTE = 0;
    
    public static final int STATUT_EMPRUNTE = 1;
}
