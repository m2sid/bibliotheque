/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class MagazineDao extends AbstractDao<Magazine> {

    private static final MagazineDao INSTANCE = new MagazineDao();

    private MagazineDao() {
    }

    public void create(Magazine magazine) {
        super.saveOrUpdate(magazine);
    }

    public void delete(Magazine magazine) {
        super.delete(magazine);
    }

    public List<Magazine> findAll() {
        super.findAll(Magazine.class);
        return super.findAll(Magazine.class);
    }

    public int size() {
        return super.size(Magazine.class);
    }

    public List<Magazine> findAllLike(String pattern) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.like("titre", "%" + pattern + "%"));
        return super.findAll(Magazine.class, criteres);
    }

    public boolean exist(String titre, Integer numero) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("titre", titre));
        criteres.add(Restrictions.eq("numero", numero));
        return size(Magazine.class, criteres) > 0;
    }

    public static MagazineDao getInstance() {
        return MagazineDao.INSTANCE;
    }
}
