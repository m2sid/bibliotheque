/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.UsagerDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class UsagerService implements Service<Usager>{
    
    public static UsagerDao usagerDao;
    
    private static final UsagerService INSTANCE = new UsagerService();
    
    private static final OeuvreService oeuvreService = ServiceFactory.getOeuvreService();
    
    private static final ReservationService reservationService = ServiceFactory.getReservationService();
    
    private UsagerService(){
        usagerDao = HibernateDaoFactory.getUsagerDao();
    }
    
    @Override
    public void create(Usager usager){
        usagerDao.saveOrUpdate(usager);
    }
    
    @Override
    public void update(Usager usager) {
        usagerDao.saveOrUpdate(usager);
    }
    
    @Override
    public void delete(Usager usager){
        Oeuvre oeuvreCourante;
        /* ---> Remplacé par un trigger
        for (Reservation reservation : reservationService.findAllByUsager(usager)) {
            oeuvreCourante = reservation.getOeuvre();
            oeuvreCourante.setNbResa(oeuvreCourante.getNbResa()-1);
            oeuvreService.update(oeuvreCourante);
        }
        */
        usagerDao.delete(usager);
    }
    
    @Override
    public List<Usager> findAll(){
        return usagerDao.findAll();
    }
    
    public List<Usager> findAllLike(String pattern){
        return usagerDao.findAllLike(pattern);
    }

    @Override
    public int size() {
     return usagerDao.size();
    }
    
    public static UsagerService getInstance(){
        return UsagerService.INSTANCE;
    }
}
