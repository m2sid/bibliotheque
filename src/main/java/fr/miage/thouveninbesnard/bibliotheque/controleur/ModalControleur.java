/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.stage.Stage;

/**
 *
 * @author Nicolas
 */
public class ModalControleur implements Initializable{
    
    public ModalControleur(){}
    
    protected boolean okCliked = false;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {}
    
    protected void closeModal(ActionEvent event) throws IOException{
        Node  source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public boolean isOkCliked() {
        return okCliked;
    }
}
