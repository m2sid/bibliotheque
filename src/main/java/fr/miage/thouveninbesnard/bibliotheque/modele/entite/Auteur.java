/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Nicolas
 */

@Entity
@Table(name = "AUTEUR")
public class Auteur extends Personne implements Serializable{
    
    private List<Livre> listelivres;
    
    public Auteur(){ }
    
    public Auteur(String nom, String prenom){
        super(nom, prenom);
    }
    
    @OneToMany(mappedBy="auteur")
    @Fetch (FetchMode.SELECT)
    public List<Livre> getListelivres() {
        return listelivres;
    }

    public void setListelivres(List<Livre> listelivres) {
        this.listelivres = listelivres;
    }
    
    public void updateModele(Auteur temp){
        nom = temp.nom;
        prenom = temp.prenom;
    }
}
