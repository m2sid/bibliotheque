/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.EmpruntService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.OeuvreService;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class EmpruntsUsagerControleur extends ModalControleur implements Initializable {
    @FXML
    private TableView<Emprunt> tablesEmprunts;
    @FXML
    private TableColumn<Emprunt, String> colonneOeuvre;
    @FXML
    private TableColumn<Emprunt, LocalDate> colonneDateEmprunt;
    @FXML
    private TableColumn<Emprunt, LocalDate> colonneDateRetour;
    
    private Usager usager;

    private ObservableList<Emprunt> listeEmprunts;
    
    private static final EmpruntService empruntService = ServiceFactory.getEmpruntService();
    
    private static final OeuvreService oeuvreService = ServiceFactory.getOeuvreService();

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (usager != null) {
            colonneOeuvre.setCellValueFactory(cellData -> cellData.getValue().getExemplaire().getOeuvre().infosOeuvre());
            colonneDateEmprunt.setCellValueFactory(cellData -> cellData.getValue().dateDebutProperty());
            colonneDateRetour.setCellValueFactory(cellData -> cellData.getValue().dateFinProperty());
            listeEmprunts = FXCollections.observableList(empruntService.findAllByUsager(usager));
            tablesEmprunts.setItems(listeEmprunts);    
        }
    }    
    
    public void setUsager(Usager usager){
        this.usager = usager;
        initialize(null, null);
    }
}
