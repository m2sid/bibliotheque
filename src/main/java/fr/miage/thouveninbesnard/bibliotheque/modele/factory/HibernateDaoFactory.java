/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.factory;

import fr.miage.thouveninbesnard.bibliotheque.modele.dao.UsagerDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.MagazineDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.LivreDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.OeuvreDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.AuteurDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.EmpruntDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.ExemplaireDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.dao.ReservationDao;

/**
 *
 * @author Nicolas
 */
public class HibernateDaoFactory {
    
    public static UsagerDao getUsagerDao(){
        return UsagerDao.getInstance();
    }
    
    public static OeuvreDao getOeuvreDao(){
        return OeuvreDao.getInstance();
    }
    
    public static LivreDao getLivreDao(){
        return LivreDao.getInstance();
    }
    
    public static MagazineDao getMagazineDao(){
        return MagazineDao.getInstance();
    }

    public static AuteurDao getAuteurDao() {
        return AuteurDao.getInstance();
    }
    
    public static ReservationDao getReservationDao(){
        return ReservationDao.getInstance();
    }
    
    public static ExemplaireDao getExemplaireDao(){
        return ExemplaireDao.getInstance();
    }
    
    public static EmpruntDao getEmpruntDao(){
        return EmpruntDao.getInstance();
    }
}
