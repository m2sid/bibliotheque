/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.util.Constantes;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class ExemplaireDao extends AbstractDao<Exemplaire> {

    private static final ExemplaireDao INSTANCE = new ExemplaireDao();

    public ExemplaireDao() {
        super();
    }

    public void create(Exemplaire exemplaire) {
        super.saveOrUpdate(exemplaire);
    }

    public void delete(Exemplaire exemplaire) {
        super.delete(exemplaire);
    }

    public List<Exemplaire> findAll() {
        return super.findAll(Exemplaire.class);
    }

    public int size() {
        return super.size(Exemplaire.class);
    }

    public int nbDispo(Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        criteres.add(Restrictions.eq("etat", Constantes.ETAT_BON));
        criteres.add(Restrictions.eq("statut", Constantes.STATUT_NON_EMPRUNTE));
        return super.size(Exemplaire.class, criteres);
    }

    public static ExemplaireDao getInstance() {
        return ExemplaireDao.INSTANCE;
    }

    public List<Exemplaire> findAllByOeuvre(Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        return super.findAll(Exemplaire.class, criteres);
    }

    public int nbExemplaireOfOeuvre(Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        criteres.add(Restrictions.eq("etat", Constantes.ETAT_BON));
        return super.size(Exemplaire.class, criteres);
    }

    public Exemplaire getExemplaireDisponible(Oeuvre oeuvre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("oeuvre", oeuvre));
        criteres.add(Restrictions.eq("etat", Constantes.ETAT_BON));
        criteres.add(Restrictions.eq("statut", Constantes.STATUT_NON_EMPRUNTE));
        return super.findOne(Exemplaire.class, criteres);
    }
}
