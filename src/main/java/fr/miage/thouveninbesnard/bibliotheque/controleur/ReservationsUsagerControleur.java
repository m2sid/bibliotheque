/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.OeuvreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ReservationService;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class ReservationsUsagerControleur extends ModalControleur implements Initializable {
    @FXML
    private TableView<Reservation> tableReservations;
    @FXML
    private TableColumn<Reservation, String> colonneOeuvre;
    @FXML
    private TableColumn<Reservation, LocalDate> colonneDate;
    @FXML
    private Button AnnulerReservation;
    @FXML
    private Button annuler;
    
    private Usager usager;

    private ObservableList<Reservation> listeReservation;
    
    private static final ReservationService reservationService = ServiceFactory.getReservationService();
    
    private static final OeuvreService oeuvreService = ServiceFactory.getOeuvreService();
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AnnulerReservation.setDisable(true);
        if (usager != null) {
            colonneOeuvre.setCellValueFactory(cellData -> cellData.getValue().getOeuvre().infosOeuvre());
            colonneDate.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
            tableReservations.getSelectionModel().selectedItemProperty().addListener((observable, precedent, courant) -> AnnulerReservation.setDisable(false));
            listeReservation = FXCollections.observableList(reservationService.findAllByUsager(usager));
            tableReservations.setItems(listeReservation);    
        }
    }    

    @FXML
    private void pressAnnulerReservation(ActionEvent event) {
        okCliked = true;
        Reservation reservation = tableReservations.getSelectionModel().getSelectedItem();
        if (reservation != null) {
            reservationService.delete(reservation);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("La réservation a été annulé");
            alert.showAndWait();
            listeReservation.remove(reservation);
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Saisir une réservation");
            alert.showAndWait();
        }
    }

    @FXML
    private void pressAnnuler(ActionEvent event) throws IOException{
        closeModal(event);
    }
    
    public void setUsager(Usager usager){
        this.usager = usager;
        initialize(null, null);
    }
}
