/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class EmpruntDao extends AbstractDao<Emprunt> {

    private static final EmpruntDao INSTANCE = new EmpruntDao();
    
    public EmpruntDao() {
        super();
    }

    public void create(Emprunt emprunt) {
        super.saveOrUpdate(emprunt);
    }

    public void delete(Emprunt emprunt) {
        super.delete(emprunt);
    }

    public List<Emprunt> findAll() {
        return super.findAll(Emprunt.class);
    }

    public int size() {
        return super.size(Emprunt.class);
    }

    public List<Emprunt> findAllByUsager(Usager usager) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("usager", usager));
        return super.findAll(Emprunt.class, criteres);
    }

    public Emprunt findEmprunt(Exemplaire exemplaire){
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("exemplaire", exemplaire));
        return super.findOne(Emprunt.class, criteres);
    }
    
    public boolean exist(Emprunt emprunt, List<Exemplaire> listeExemplaires){
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.in("exemplaire", listeExemplaires));
        criteres.add(Restrictions.eq("usager", emprunt.getUsager()));
        return size(Emprunt.class, criteres) > 0;
    }
    
    public static EmpruntDao getInstance() {
        return EmpruntDao.INSTANCE;
    }
}
