/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.AuteurService;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.PersonneStringConverter;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.LivreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Nicolas
 */
public class AjouterLivreControleur implements Initializable {

    @FXML
    private TextField titre;

    @FXML
    private TextArea resume;

    @FXML
    private ComboBox auteurComboBox;

    @FXML
    private Button ajouter;

    @FXML
    private Button creer;

    @FXML
    private AnchorPane pane;

    private static AuteurService auteurService = ServiceFactory.getAuteurService();

    private static LivreService livreService = ServiceFactory.getLivreService();

    private ObservableList<Auteur> listeAuteurs;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listeAuteurs = FXCollections.observableList((List<Auteur>) auteurService.findAll());
        auteurComboBox.setConverter(new PersonneStringConverter());
        auteurComboBox.setItems(listeAuteurs);
    }

    @FXML
    private void pressAjouter() throws IOException {
        Auteur auteur = (Auteur) auteurComboBox.getSelectionModel().getSelectedItem();
        String titreStr = titre.getText();
        String resumeStr = resume.getText();
        if (checkData(titreStr, auteur)) {
            Livre livre = new Livre(titreStr, resumeStr, auteur);
            if (livreService.exist(livre)) {

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle(null);
                alert.setHeaderText(null);
                alert.setContentText(titre + " existe déjà");
                alert.showAndWait();

            } else {
                livreService.create(livre);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle(null);
                alert.setHeaderText(null);
                alert.setContentText(titreStr + " créé");
                alert.showAndWait();
                BibliothequeApp.getInstance().changerScene("/fxml/ListerOeuvre.fxml");
            }

        }
    }

    @FXML
    private void pressCreer() throws IOException {
        Auteur auteur = new Auteur();
        boolean okCliked = BibliothequeApp.getInstance().ouvrirModal("/fxml/ModifierAuteur.fxml", (loader) -> {
            ModifierAuteurControleur controleur = loader.getController();
            controleur.setAuteur(auteur);
            return controleur;
        });
        if (okCliked) {
            auteurService.create(auteur);
            listeAuteurs.add(auteur);
            auteurComboBox.getSelectionModel().select(auteur);
        }
    }

    private boolean checkData(String titre, Auteur auteur) {
        return checkTitre(titre) && checkAuteur(auteur);
    }

    private boolean checkTitre(String titre) {
        if (titre.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Aucun titre spécifié");
            alert.showAndWait();
            return false;
        }
        return true;
    }

    private boolean checkAuteur(Auteur auteur) {
        if (auteur == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText("Aucun auteur spécifié");
            alert.showAndWait();
            return false;
        }
        return true;
    }
}
