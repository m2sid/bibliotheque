/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.UsagerService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class AjouterUsagerControleur implements Initializable {
    
    @FXML
    private TextField nom;
    
    @FXML
    private TextField prenom;
    
    @FXML
    private DatePicker dateNaissance;
    
    @FXML
    private TextField adresse;
    
    @FXML
    private TextField mail;
    
    @FXML
    private TextField telephone;
    
    @FXML
    private Button enregistrer;
    
    private final UsagerService usagerService = ServiceFactory.getUsagerService();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    
    @FXML
    private void pressEnregistrer(){
        Usager nouveauUsager = new Usager(nom.getText(), prenom.getText(), dateNaissance.getValue(), adresse.getText(), mail.getText(), telephone.getText());
        usagerService.create(nouveauUsager);
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(null);
        alert.setHeaderText(null);
        alert.setContentText(nouveauUsager.getNom()+" "+nouveauUsager.getNom()+" enregistré");
        alert.showAndWait();
    }
}
