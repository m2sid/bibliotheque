/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.dao.EmpruntDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class EmpruntService implements Service<Emprunt> {

    private static EmpruntDao empruntDao;

    private static final EmpruntService INSTANCE = new EmpruntService();

    private static final ReservationService reservationService = ServiceFactory.getReservationService();
    
    private static final ExemplaireService exemplaireService = ServiceFactory.getExemplaireService();

    private EmpruntService() {
        empruntDao = HibernateDaoFactory.getEmpruntDao();
    }

    @Override
    public void create(Emprunt emprunt) {
        empruntDao.saveOrUpdate(emprunt);
        Usager usager = emprunt.getUsager();
        Oeuvre oeuvre = emprunt.getExemplaire().getOeuvre();
        Reservation reservation = reservationService.findOneBy(usager, oeuvre);
        if (reservation != null) {
            reservationService.delete(reservation);
        }
    }

    @Override
    public void update(Emprunt emprunt) {
        empruntDao.saveOrUpdate(emprunt);
    }

    @Override
    public void delete(Emprunt emprunt) {
        empruntDao.delete(emprunt);
    }

    @Override
    public List<Emprunt> findAll() {
        return empruntDao.findAll();
    }

    @Override
    public int size() {
        return empruntDao.size();
    }

    public List<Emprunt> findAllByUsager(Usager usager) {
        return empruntDao.findAllByUsager(usager);
    }
    
    public Emprunt findEmprunt(Exemplaire exemplaire){
        return empruntDao.findEmprunt(exemplaire);
    }
    
    public boolean exist(Emprunt emprunt){
        Oeuvre oeuvre = emprunt.getExemplaire().getOeuvre();
        List<Exemplaire> listeExemplaires = exemplaireService.findAllByOeuvre(oeuvre);
        return empruntDao.exist(emprunt, listeExemplaires);
    }

    public static EmpruntService getInstance() {
        return EmpruntService.INSTANCE;
    }
}
