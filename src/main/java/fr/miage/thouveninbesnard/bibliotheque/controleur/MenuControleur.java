/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class MenuControleur implements Initializable {
    @FXML
    private Button ajouterUsager;
    @FXML
    private Button listerUsager;
    @FXML
    private Button ajouterLivre;
    @FXML
    private Button ajouterMagazine;
    @FXML
    private BorderPane border;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    @FXML
    private void pressAjouterUsager(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterUsager.fxml");
    }
    
    @FXML
    private void pressListerUsager(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/ListerUsager.fxml");
    }
    
    @FXML
    private void pressAjouterLivre(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterLivre.fxml");
    }
    
    @FXML
    private void pressAjouterMagazine(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/AjouterMagazine.fxml");
    }
    
    @FXML
    private void pressListerOeuvre(ActionEvent event) throws IOException{
        BibliothequeApp.getInstance().changerScene("/fxml/ListerOeuvre.fxml");
    }
}
