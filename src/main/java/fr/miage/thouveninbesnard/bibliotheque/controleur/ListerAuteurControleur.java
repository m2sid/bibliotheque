/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.AuteurService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.LivreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class ListerAuteurControleur implements Initializable {
    @FXML
    private TableView<Auteur> tableAuteurs;
    @FXML
    private TableColumn<Auteur, String> colonneNomAuteur;
    @FXML
    private TableColumn<Auteur, String> colonnePrenomAuteur;
    @FXML
    private Button modifierAuteur;
    @FXML
    private TableView<Livre> tableLivres;
    @FXML
    private TableColumn<Livre, String> colonneLivre;
    @FXML
    private TableColumn<Livre, String> colonneReservation;
    
    private ObservableList<Auteur> listeAuteurs;
    
    private ObservableList<Livre> listeLivres;
    
    private static final AuteurService auteurService = ServiceFactory.getAuteurService();
    
    private static final LivreService livreService = ServiceFactory.getLivreService();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colonneNomAuteur.setCellValueFactory(cellData -> cellData.getValue().nomProperty());
        colonnePrenomAuteur.setCellValueFactory(cellData -> cellData.getValue().prenomProperty());
        
        colonneLivre.setCellValueFactory(cellData -> cellData.getValue().titreProperty());
        colonneReservation.setCellValueFactory(cellData -> cellData.getValue().nbResaProperty());
        
        tableAuteurs.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherAuteur(courant));
        
        listeAuteurs = FXCollections.observableList((List<Auteur>) auteurService.findAll());
        tableAuteurs.setItems(listeAuteurs);
        modifierAuteur.setDisable(true);
    }    

    @FXML
    private void afficherAuteur(Auteur courant) {
        listeLivres = FXCollections.observableList(livreService.findAllByAuteur(courant));
        tableLivres.setItems(listeLivres);
        modifierAuteur.setDisable(false);
    }
    
    @FXML
    private void pressModifierAuteur(ActionEvent event) throws IOException{
        int auteurIndex = tableAuteurs.getSelectionModel().getSelectedIndex();
        Auteur auteur = listeAuteurs.get(auteurIndex);
        BibliothequeApp.getInstance().ouvrirModal("/fxml/ModifierAuteur.fxml", (loader) -> {
           ModifierAuteurControleur controleur = loader.getController();
           controleur.setAuteur(auteur);
           return controleur;
        });
        tableAuteurs.getColumns().get(0).setVisible(false);
        tableAuteurs.getColumns().get(0).setVisible(true);
    }
}
