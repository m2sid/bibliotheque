package fr.miage.thouveninbesnard.bibliotheque.modele.factory;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Personne;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nicolas
 */
public class HibernateFactory {

    private static SessionFactory sessionFactory;

    private HibernateFactory() {
        createSessionFactory();
    }

    private static void createSessionFactory() throws HibernateException {
        if (sessionFactory != null) {
            closeFactory();
        }
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(Reservation.class);
        configuration.addAnnotatedClass(Personne.class);
        configuration.addAnnotatedClass(Oeuvre.class);
        configuration.addAnnotatedClass(Usager.class);
        configuration.addAnnotatedClass(Livre.class);
        configuration.addAnnotatedClass(Magazine.class);
        configuration.addAnnotatedClass(Auteur.class);
        configuration.addAnnotatedClass(Exemplaire.class);
        configuration.addAnnotatedClass(Emprunt.class);
        sessionFactory = configuration.buildSessionFactory();
    }

    public static SessionFactory getSessionFactory() throws HibernateException {
        if (sessionFactory == null) {
            createSessionFactory();
        }
        return sessionFactory;
    }

    public static void close(Session session) {
        session.close();
    }

    public static void closeFactory() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}
