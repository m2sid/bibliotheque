/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.dao.AuteurDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class AuteurService implements Service<Auteur> {

    private static AuteurDao auteurDao;

    private static final AuteurService INSTANCE = new AuteurService();

    private AuteurService() {
        auteurDao = HibernateDaoFactory.getAuteurDao();
    }

    @Override
    public void create(Auteur auteur) {
        auteurDao.saveOrUpdate(auteur);
    }

    @Override
    public void update(Auteur auteur) {
        auteurDao.saveOrUpdate(auteur);
    }

    @Override
    public void delete(Auteur auteur) {
        auteurDao.delete(auteur);
    }

    @Override
    public List<Auteur> findAll() {
        return auteurDao.findAll();
    }

    @Override
    public int size() {
        return auteurDao.size();
    }

    public boolean exist(Auteur auteur) {
        return auteurDao.exist(auteur);
    }

    public static AuteurService getInstance() {
        return AuteurService.INSTANCE;
    }

}
