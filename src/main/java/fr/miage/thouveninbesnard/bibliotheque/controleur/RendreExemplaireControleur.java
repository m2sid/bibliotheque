/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.EmpruntService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ExemplaireService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.NotificationService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.UsagerService;
import fr.miage.thouveninbesnard.bibliotheque.util.Constantes;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javax.mail.MessagingException;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class RendreExemplaireControleur implements Initializable {

    @FXML
    private TableView<Usager> tableUsager;
    @FXML
    private TableColumn<Usager, String> colonneUsager;
    @FXML
    private TextField filtreUsager;
    @FXML
    private Button buttonFiltrerUsager;
    @FXML
    private TableView<Emprunt> tableEmprunt;
    @FXML
    private TableColumn<Emprunt, String> colonneTitre;
    @FXML
    private TableColumn<Emprunt, LocalDate> colonneDateEmprunt;
    @FXML
    private TableColumn<Emprunt, LocalDate> colonneDateRendu;
    @FXML
    private Button buttonSuivant;

    private static final UsagerService usagerService = ServiceFactory.getUsagerService();

    private static final EmpruntService empruntService = ServiceFactory.getEmpruntService();

    private static final ExemplaireService exemplaireService = ServiceFactory.getExemplaireService();

    private static final NotificationService notificationService = ServiceFactory.getNotificationService();

    private ObservableList<Usager> listeUsagers;

    private ObservableList<Emprunt> listeEmprunt;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colonneUsager.setCellValueFactory(cellData -> cellData.getValue().personneProperty());
        listeUsagers = FXCollections.observableList((List<Usager>) usagerService.findAll());
        tableUsager.setItems(listeUsagers);
        tableUsager.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherEmprunt(courant));

        colonneDateEmprunt.setCellValueFactory(cellData -> cellData.getValue().dateDebutProperty());
        colonneDateRendu.setCellValueFactory(cellData -> cellData.getValue().dateFinProperty());
        colonneTitre.setCellValueFactory(cellData -> cellData.getValue().getExemplaire().getOeuvre().titreProperty());
    }

    private void afficherEmprunt(Usager courant) {
        listeEmprunt = FXCollections.observableList((List<Emprunt>) empruntService.findAllByUsager(courant));
        tableEmprunt.setItems(listeEmprunt);
    }

    @FXML
    private void filtrerUsager(Event event) {
        String pattern = filtreUsager.getText();
        listeUsagers = FXCollections.observableList((List<Usager>) usagerService.findAllLike(pattern));
        tableUsager.setItems(null);
        tableUsager.setItems(listeUsagers);
    }

    @FXML
    private void pressSuivant(ActionEvent event) {
        Emprunt emprunt = tableEmprunt.getSelectionModel().getSelectedItem();
        Usager usager = tableUsager.getSelectionModel().getSelectedItem();
        if (emprunt != null && usager != null) {
            afficherChoixEtat(emprunt);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur rendu");
            alert.setHeaderText(null);
            alert.setContentText("Un usager et une oeuvre doivent être renseignés");
            alert.showAndWait();
        }
    }

    private void afficherChoixEtat(Emprunt emprunt) {
        Exemplaire exemplaire = emprunt.getExemplaire();

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Rendre Exemplaire");
        alert.setHeaderText("Quel est l'état de l'exemplaire ?");
        alert.setContentText("Infos . \n - Bon : L'exemplaire peut être remis à disposition \n - Abimé : Mettre de côté l'exemplaire pour l'Atelier \n - Périmé : l'exemplaire ne doit plus être proposé\n \n");

        ButtonType buttonTypeBon = new ButtonType("Bon");
        ButtonType buttonTypeAbime = new ButtonType("Abimé");
        ButtonType buttonTypePerime = new ButtonType("Périmé");
        ButtonType buttonTypeCancel = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeBon, buttonTypeAbime, buttonTypePerime, buttonTypeCancel);

        if (exemplaire.getEtat() != Constantes.ETAT_BON) {
            Node etatBonButton = alert.getDialogPane().lookupButton(buttonTypeBon);
            etatBonButton.setDisable(true);
        }

        Optional<ButtonType> result = alert.showAndWait();
        ButtonType choix = result.get();

        if (choix != buttonTypeCancel) {
            empruntService.delete(emprunt);
            if (choix == buttonTypeBon) {
                try {
                    notificationService.notifier(exemplaire.getOeuvre());
                } catch (MessagingException ex) {
                    Alert alertMessage = new Alert(Alert.AlertType.ERROR);
                    alertMessage.setTitle("Erreur notification");
                    alertMessage.setHeaderText(null);
                    alertMessage.setContentText("La notification de disponibilité à echoué");
                    alertMessage.showAndWait();
                    Logger.getLogger(RendreExemplaireControleur.class.getName()).log(Level.SEVERE, "La notification de disponibilité à echoué", ex);
                }
            } else if (choix == buttonTypeAbime) {
                exemplaire.setEtat(Constantes.ETAT_ABIME);
                exemplaireService.update(exemplaire);
            } else if (choix == buttonTypePerime) {
                exemplaireService.delete(exemplaire);
            }
            Alert alertMessage = new Alert(Alert.AlertType.INFORMATION);
            alertMessage.setTitle("Emprunt supprimé");
            alertMessage.setHeaderText(null);
            alertMessage.setContentText("L'exemplaire à bien été rendu");
            alertMessage.showAndWait();
        }
    }

}
