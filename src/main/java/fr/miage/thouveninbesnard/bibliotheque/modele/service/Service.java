/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import java.util.List;


/**
 *
 * @author Nicolas
 */
public interface Service<E> {
    
    public void create(E entity);
    
    public void update(E entity);
    
    public void delete(E entity);
    
    public List<E> findAll();
    
    public int size();        
}
