/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.dao.OeuvreDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class OeuvreService implements Service<Oeuvre>{

    private OeuvreDao oeuvreDao;
    
    private static final OeuvreService INSTANCE = new OeuvreService();
    
    private OeuvreService(){
        oeuvreDao = HibernateDaoFactory.getOeuvreDao();
    }
    
    @Override
    public void create(Oeuvre entity) {
        oeuvreDao.saveOrUpdate(entity);
    }

    @Override
    public void update(Oeuvre entity) {
        oeuvreDao.saveOrUpdate(entity);
    }

    @Override
    public void delete(Oeuvre entity) {
        oeuvreDao.delete(entity);
    }

    @Override
    public List<Oeuvre> findAll() {
        return oeuvreDao.findAll();
    }

    @Override
    public int size() {
        return oeuvreDao.size();
    }
    
    public void incrNbResa(Oeuvre oeuvre){
        
    }
    
    public static OeuvreService getInstance(){
        return OeuvreService.INSTANCE;
    }
}
