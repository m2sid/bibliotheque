/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import fr.miage.thouveninbesnard.bibliotheque.util.Constantes;
import fr.miage.thouveninbesnard.bibliotheque.util.DateUtil;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Nicolas
 */
@Entity
@Table(name = "EMPRUNT")
public class Emprunt implements Serializable {

    private IntegerProperty id = new SimpleIntegerProperty();

    private ObjectProperty<Date> dateDebut = new SimpleObjectProperty();

    private ObjectProperty<Date> dateFin = new SimpleObjectProperty();

    private Exemplaire exemplaire;

    private Usager usager;

    public Emprunt() {
        Date date = new Date();
        this.dateDebut.setValue(date);
        this.dateFin.setValue(DateUtil.datePlus(date, Constantes.DUREE_EMPRUNT));
    }

    public Emprunt(Usager usager, Exemplaire exemplaire) {
        Date date = new Date();
        this.dateDebut.setValue(date);
        this.dateFin.setValue(DateUtil.datePlus(date, Constantes.DUREE_EMPRUNT));
        this.usager = usager;
        this.exemplaire = exemplaire;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return this.id.getValue();
    }

    public void setId(Integer id) {
        this.id.setValue(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "dateDebut")
    public Date getDateDebut() {
        return dateDebut.getValue();
    }

    public void setDateDebut(Date date) {
        this.dateDebut.setValue(date);
    }

    public ObjectProperty<LocalDate> dateDebutProperty() {
        return new SimpleObjectProperty<>(DateUtil.asLocalDate(dateDebut.getValue()));
    }

    @Column(name = "dateFin")
    public Date getDateFin() {
        return dateFin.getValue();
    }

    public void setDateFin(Date date) {
        this.dateFin.setValue(date);
    }

    public ObjectProperty<LocalDate> dateFinProperty() {
        return new SimpleObjectProperty<>(DateUtil.asLocalDate(dateFin.getValue()));
    }

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    @JoinColumn(name = "exemplaire_id")
    public Exemplaire getExemplaire() {
        return exemplaire;
    }

    public void setExemplaire(Exemplaire exemplaire) {
        this.exemplaire = exemplaire;
    }

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    @JoinColumn(name = "usager_id")
    public Usager getUsager() {
        return usager;
    }

    public void setUsager(Usager usager) {
        this.usager = usager;
    }
}
