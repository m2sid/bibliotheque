/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import fr.miage.thouveninbesnard.bibliotheque.util.Constantes;
import java.io.Serializable;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Nicolas
 */
@Entity
@Table(name = "EXEMPLAIRE")
public class Exemplaire implements Serializable {

    private IntegerProperty id = new SimpleIntegerProperty();

    private IntegerProperty etat = new SimpleIntegerProperty();

    private IntegerProperty statut = new SimpleIntegerProperty();
    
    private Oeuvre oeuvre;

    private List<Emprunt> listeEmprunts;
    
    public Exemplaire() {
        this.etat.set(Constantes.ETAT_BON);
        this.statut.set(Constantes.STATUT_NON_EMPRUNTE);
    }

    public Exemplaire(Oeuvre oeuvre) {
        this.etat.set(Constantes.ETAT_BON);
        this.statut.set(Constantes.STATUT_NON_EMPRUNTE);
        this.oeuvre = oeuvre;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return this.id.getValue();
    }

    public void setId(Integer id) {
        this.id.setValue(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Column(name = "etat")
    public Integer getEtat() {
        return this.etat.getValue();
    }

    public void setEtat(Integer etat) {
        this.etat.setValue(etat);
    }

    public IntegerProperty etatProperty() {
        return etat;
    }
    
    @Column(name = "statut")
    public Integer getStatut() {
        return this.statut.getValue();
    }

    public void setStatut(Integer statut) {
        this.statut.setValue(statut);
    }

    public IntegerProperty statutProperty() {
        return statut;
    }

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    @JoinColumn(name = "oeuvre_id")
    public Oeuvre getOeuvre() {
        return oeuvre;
    }

    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }
    
    @OneToMany(mappedBy="exemplaire")
    @Fetch (FetchMode.SELECT)
    public List<Emprunt> getListeEmprunts() {
        return listeEmprunts;
    }
    
    public void setListeEmprunts(List<Emprunt> listeEmprunts) {
        this.listeEmprunts = listeEmprunts;
    }
}
