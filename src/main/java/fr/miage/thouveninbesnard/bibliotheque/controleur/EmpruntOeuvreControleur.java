/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Emprunt;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.EmpruntService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ExemplaireService;
import fr.miage.thouveninbesnard.bibliotheque.util.Constantes;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class EmpruntOeuvreControleur extends ModalControleur implements Initializable {

    @FXML
    private TableColumn<Exemplaire, String> colonneDateFin;
    @FXML
    private TableView<Exemplaire> tableEmprunts;
    @FXML
    private TableColumn<Exemplaire, String> colonneDispo;
    @FXML
    private TableColumn<Exemplaire, String> colonneUsager;

    private static final ExemplaireService exemplaireService = ServiceFactory.getExemplaireService();

    private static final EmpruntService empruntService = ServiceFactory.getEmpruntService();
    
    private ObservableList<Exemplaire> listeExemplaires;

    private Oeuvre oeuvre;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (oeuvre != null) {
            colonneDispo.setCellValueFactory(cellData -> new SimpleStringProperty((cellData.getValue().getStatut() == Constantes.STATUT_EMPRUNTE)?"Emprunté":"Dispo"));
            colonneUsager.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Exemplaire, String>, ObservableValue<String>>() {

                public ObservableValue<String> call(TableColumn.CellDataFeatures<Exemplaire, String> cellData) {
                    Emprunt emprunt = empruntService.findEmprunt(cellData.getValue());
                    return (emprunt == null)? new SimpleStringProperty( "-") : emprunt.getUsager().personneProperty();
                }
            });
            
            colonneDateFin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Exemplaire, String>, ObservableValue<String>>() {

                public ObservableValue<String> call(TableColumn.CellDataFeatures<Exemplaire, String> cellData) {
                    Exemplaire exemplaire;
                    Emprunt emprunt = empruntService.findEmprunt(cellData.getValue());
                    return (emprunt == null)? new SimpleStringProperty("-") : emprunt.dateFinProperty().asString();
                }
            });
            listeExemplaires = FXCollections.observableList(exemplaireService.findAllByOeuvre(oeuvre));
            tableEmprunts.setItems(listeExemplaires);    
        }
    }

    public void setOeuvre(Oeuvre oeuvre){
        this.oeuvre = oeuvre;
        initialize(null, null);
    }
}
