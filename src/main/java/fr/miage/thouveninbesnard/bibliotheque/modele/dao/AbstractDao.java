/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateFactory;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;

/**
 *
 * @author Nicolas
 * @param <E>
 */
public abstract class AbstractDao<E> {
    
    protected Session session;
    
    protected Transaction transaction;
    
    public AbstractDao(){
    }
    
    public void startOperation() throws HibernateException{
        session = HibernateFactory.getSessionFactory().openSession();
        transaction = session.beginTransaction();
    }
    
    public void saveOrUpdate(Object o){
        try{
            startOperation();
            session.saveOrUpdate(o);
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
           HibernateFactory.close(session);
        }
    }
    
    public void delete(Object o){
        try{
            startOperation();
            session.delete(o);
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);
        }
    }
    
    public List<E> findAll(Class aClass) {
        List list = new  ArrayList();
        try{
            startOperation();
            list = session.createCriteria(aClass).list();
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);
        }
        return list;
    }

    public List<E> findAll(Class aClass, List<Criterion> criteres) {
        List list = new  ArrayList();
        try{
            startOperation();
            Criteria criteria = session.createCriteria(aClass);
            for (Criterion critere : criteres) {
                criteria.add(critere);
            }
            list = criteria.list();
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);
        }
        return list;
    }
    
        public E findOne(Class aClass) {
        E entity = null;
        try{
            startOperation();
            entity = (E) session.createCriteria(aClass).setMaxResults(1).uniqueResult();
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);
        }
        return entity;
    }

    public E findOne(Class aClass, List<Criterion> criteres) {
        E entity = null;
        try{
            startOperation();
            Criteria criteria = session.createCriteria(aClass);
            for (Criterion critere : criteres) {
                criteria.add(critere);
            }
            entity = (E) criteria.setMaxResults(1).uniqueResult();
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);
        }
        return entity;
    }
    
    public int size(Class<?> aClass) {
        Number nbElement = 0;
        try{
            startOperation();
            nbElement = (Number) session.createCriteria(aClass).setProjection(Projections.rowCount()).uniqueResult();
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);   
        }
        return nbElement.intValue();
    }
    
    public int size(Class<?> aClass, List<Criterion> criteres) {
        Number nbElement = 0;
        try{
            startOperation();
            Criteria criteria = session.createCriteria(aClass);
            for (Criterion critere : criteres) {
                criteria.add(critere);
            }
            nbElement = (Number) criteria.setProjection(Projections.rowCount()).uniqueResult();
            transaction.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }finally{
            HibernateFactory.close(session);   
        }
        return nbElement.intValue();
    }
}
