/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Nicolas
 */

@Entity
@Table(name = "OEUVRE")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Oeuvre {
    protected IntegerProperty id = new SimpleIntegerProperty();
    
    protected StringProperty titre = new SimpleStringProperty();
    
    protected IntegerProperty nbResa = new SimpleIntegerProperty(0);
    
    protected List<Reservation> listeReservations;
    
    protected List<Exemplaire> listeExemplaires;
    
    public Oeuvre(){}
    
    public Oeuvre(String titre){
        this.titre.setValue(titre);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "oeuvre_id")
    public Integer getId() {
        return this.id.getValue();
    }
    
    public void setId(Integer id){
        this.id.setValue(id);
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    @Column(name = "titre")
    public String getTitre() {
        return titre.getValue();
    }
    
    public void setTitre(String titre){
        this.titre.setValue(titre);
    }
    
    public StringProperty titreProperty() {
        return titre;
    }
    
    @Column(name = "nbResa")
    public Integer getNbResa() {
        return this.nbResa.getValue();
    }
    
    public void setNbResa(Integer nbResa){
        this.nbResa.setValue(nbResa);
    }
    
    public StringProperty nbResaProperty() {
        return new SimpleStringProperty(String.valueOf(nbResa.getValue()));
    }
    
    public abstract StringProperty infosOeuvre();
    
    @OneToMany(mappedBy="oeuvre")
    @Fetch (FetchMode.SELECT)
    public List<Reservation> getListeReservations() {
        return listeReservations;
    }
    
    public void setListeReservations(List<Reservation> listeReservations) {
        this.listeReservations = listeReservations;
    }
    
    @OneToMany(mappedBy="oeuvre")
    @Fetch (FetchMode.SELECT)
    public List<Exemplaire> getListeExemplaires() {
        return listeExemplaires;
    }
    
    public void setListeExemplaires(List<Exemplaire> listeExemplaires) {
        this.listeExemplaires = listeExemplaires;
    }
}