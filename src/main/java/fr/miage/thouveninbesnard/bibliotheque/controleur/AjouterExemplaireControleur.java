/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ExemplaireService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.LivreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.MagazineService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.NotificationService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.OeuvreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ReservationService;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.mail.MessagingException;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class AjouterExemplaireControleur implements Initializable {

    @FXML
    private AnchorPane anchorPaneOeuvre;
    @FXML
    private Label nombreReservation;
    @FXML
    private Label nombreExemplaire;
    @FXML
    private Label nombreReservationValue;
    @FXML
    private Label nombreExemplaireValue;
    @FXML
    private TextField nombreNouvelleReservation;
    @FXML
    private Button buttonAjouter;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabLivre;
    @FXML
    private TableView<Livre> tableLivre;
    @FXML
    private TableColumn<Livre, String> colonneTitreLivre;
    @FXML
    private Tab tabMagazine;
    @FXML
    private TableView<Magazine> tableMagazine;
    @FXML
    private TableColumn<Magazine, String> colonneTitreMagazine;
    @FXML
    private TableColumn<Magazine, String> colonneNumeroMagazine;
    @FXML
    private Button buttonFiltrerOeuvre;
    @FXML
    private TextField filtreOeuvre;

    private static final OeuvreService oeuvreService = ServiceFactory.getOeuvreService();

    private static final LivreService livreService = ServiceFactory.getLivreService();

    private static final MagazineService magazineService = ServiceFactory.getMagazineService();

    private static final ExemplaireService exemplaireService = ServiceFactory.getExemplaireService();

    private static final ReservationService reservationService = ServiceFactory.getReservationService();

    private static final NotificationService notificationService = ServiceFactory.getNotificationService();

    private ObservableList<Magazine> listeMagazines;

    private ObservableList<Livre> listeLivres;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colonneNumeroMagazine.setCellValueFactory(cellData -> cellData.getValue().numeroProperty());
        colonneTitreLivre.setCellValueFactory(cellData -> cellData.getValue().titreProperty());
        colonneTitreMagazine.setCellValueFactory(cellData -> cellData.getValue().titreProperty());

        tableLivre.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherOeuvre(courant));

        tableMagazine.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherOeuvre(courant));

        listeLivres = FXCollections.observableList((List<Livre>) livreService.findAll());
        listeMagazines = FXCollections.observableList((List<Magazine>) magazineService.findAll());

        tableLivre.setItems(listeLivres);
        tableMagazine.setItems(listeMagazines);
    }

    private void afficherOeuvre(Oeuvre courant) {
        nombreReservationValue.setText("" + reservationService.getNbResaOfOeuvre(courant));
        nombreExemplaireValue.setText("" + exemplaireService.nbDispo(courant) + "/" + exemplaireService.nbExemplaireOfOeuvre(courant));
        buttonAjouter.setDisable(false);
        nombreNouvelleReservation.setDisable(false);
    }

    @FXML
    private void pressAjouterExemplaire(ActionEvent event) {
        try {
            int nb = Integer.parseInt(nombreNouvelleReservation.getText());
            if (nb > 0) {
                Exemplaire exemplaire;
                Tab tab = tabPane.getSelectionModel().getSelectedItem();
                Oeuvre oeuvre = (tab.equals(tabLivre)) ? tableLivre.getSelectionModel().getSelectedItem() : tableMagazine.getSelectionModel().getSelectedItem();
                for (int i = 0; i < nb; i++) {
                    exemplaire = new Exemplaire(oeuvre);
                    exemplaireService.create(exemplaire);
                }
                afficherOeuvre(oeuvre);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ajout d'exemplaire");
                alert.setHeaderText(null);
                alert.setContentText(nb + " exemplaires ont été ajouté à " + oeuvre.getTitre());
                alert.showAndWait();
                notifierUsager(oeuvre);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ajout d'exemplaire");
                alert.setHeaderText(null);
                alert.setContentText("Saisir un nombre d'exemplaire valide");
                alert.showAndWait();
            }
        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ajout d'exemplaire");
            alert.setHeaderText(null);
            alert.setContentText("Saisir un nombre");
            alert.showAndWait();
        }
    }

    @FXML
    private void filtrerOeuvre(Event event) {
        String pattern = filtreOeuvre.getText();
        listeLivres = FXCollections.observableList((List<Livre>) livreService.findAllLike(pattern));
        tableLivre.setItems(null);
        tableLivre.setItems(listeLivres);

        listeMagazines = FXCollections.observableList((List<Magazine>) magazineService.findAllLike(pattern));
        tableMagazine.setItems(null);
        tableMagazine.setItems(listeMagazines);
    }

    private void notifierUsager(Oeuvre oeuvre) {
        try {
            notificationService.notifier(oeuvre);
        } catch (MessagingException ex) {
            Alert alertMessage = new Alert(Alert.AlertType.ERROR);
            alertMessage.setTitle("Erreur notification");
            alertMessage.setHeaderText(null);
            alertMessage.setContentText("La notification de disponibilité à echoué");
            alertMessage.showAndWait();
            Logger.getLogger(RendreExemplaireControleur.class.getName()).log(Level.SEVERE, "La notification de disponibilité à echoué", ex);
        }
    }
}
