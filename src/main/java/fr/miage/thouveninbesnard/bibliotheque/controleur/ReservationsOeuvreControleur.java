/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ReservationService;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class ReservationsOeuvreControleur extends ModalControleur implements Initializable {
    @FXML
    private TableView<Reservation> tableReservations;
    @FXML
    private TableColumn<Reservation, String> colonneUsager;
    @FXML
    private TableColumn<Reservation, LocalDate> colonneDateReservation;

    private static final ReservationService reservationService = ServiceFactory.getReservationService();
    
    private ObservableList<Reservation> listeReservation;
    
    private Oeuvre oeuvre;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (oeuvre != null) {
            colonneUsager.setCellValueFactory(cellData -> cellData.getValue().getUsager().personneProperty());
            colonneDateReservation.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
            listeReservation = FXCollections.observableList(reservationService.findAllByOeuvre(oeuvre));
            tableReservations.setItems(listeReservation);    
        }
    }  
    
    public void setOeuvre(Oeuvre oeuvre){
        this.oeuvre = oeuvre;
        initialize(null, null);
    }
}
