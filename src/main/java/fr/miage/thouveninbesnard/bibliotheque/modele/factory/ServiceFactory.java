/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.modele.factory;

import fr.miage.thouveninbesnard.bibliotheque.modele.service.AuteurService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.EmpruntService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ExemplaireService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.LivreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.MagazineService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.NotificationService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.OeuvreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ReservationService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.UsagerService;

/**
 *
 * @author Nicolas
 */
public class ServiceFactory {
    
    public static UsagerService getUsagerService(){
        return UsagerService.getInstance();
    }
    
    public static OeuvreService getOeuvreService(){
        return OeuvreService.getInstance();
    }
    
    public static LivreService getLivreService(){
        return LivreService.getInstance();
    }
    
    public static MagazineService getMagazineService(){
        return MagazineService.getInstance();
    }
    
    public static AuteurService getAuteurService(){
        return AuteurService.getInstance();
    }
    
    public static ReservationService getReservationService(){
        return ReservationService.getInstance();
    }
    
    public static ExemplaireService getExemplaireService(){
        return ExemplaireService.getInstance();
    }
    
    public static EmpruntService getEmpruntService(){
        return EmpruntService.getInstance();
    }
    
    public static NotificationService getNotificationService(){
        return NotificationService.getInstance();
    }
}