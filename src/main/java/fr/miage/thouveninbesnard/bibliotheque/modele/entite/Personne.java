/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import java.io.Serializable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author Nicolas
 */

@Entity
@Table(name = "PERSONNE")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Personne implements Serializable{
    
    protected IntegerProperty id = new SimpleIntegerProperty();
    
    protected StringProperty nom = new SimpleStringProperty();
    
    protected StringProperty prenom = new SimpleStringProperty();
    
    public Personne(){ }
    
    public Personne(String nom, String prenom){
        this.nom.setValue(nom);
        this.prenom.setValue(prenom);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "personne_id")
    public Integer getId() {
        return this.id.getValue();
    }
    
    public void setId(Integer id){
        this.id.setValue(id);
    }
    
    public IntegerProperty idProperty() {
        return id;
    }
    
    @Column(name = "nom")
    public String getNom() {
        return nom.getValue();
    }
    
    public void setNom(String nom){
        this.nom.setValue(nom);
    }
    
    public StringProperty nomProperty() {
        return nom;
    }
    
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom.getValue();
    }
    
    
    public void setPrenom(String prenom){
        this.prenom.setValue(prenom);
    }
    
    public StringProperty prenomProperty() {
        return prenom;
    }
    
    public StringProperty personneProperty(){
        return new SimpleStringProperty(prenom.getValue()+" "+nom.getValue());
    }
}
