package fr.miage.thouveninbesnard.bibliotheque;

import fr.miage.thouveninbesnard.bibliotheque.controleur.ModalControleur;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateFactory;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.mail.MessagingException;


public class BibliothequeApp extends Application {
    
    private Stage primaryStage;
    private BorderPane rootLayout;
    
    private static BibliothequeApp INSTANCE;
    
    public static BibliothequeApp getInstance(){
        return INSTANCE;
    }
    
    public static void main(String[] args) throws MessagingException {       
        HibernateFactory.getSessionFactory();
        launch(args);
        HibernateFactory.closeFactory();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
       
        this.primaryStage = stage;
        this.primaryStage.setTitle("Bibliothèque");
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/RootLayout.fxml"));
        this.rootLayout = (BorderPane) loader.load();
        
        FXMLLoader loader2 = new FXMLLoader();
        loader2.setLocation(getClass().getResource("/fxml/Accueil.fxml"));
        this.rootLayout.setCenter(loader2.load());
        
        Scene scene = new Scene(this.rootLayout);
        this.primaryStage.setScene(scene);
        this.primaryStage.show();
        
        BibliothequeApp.INSTANCE = this;
    }
    
    public void changerScene(String url) throws IOException {
        changerScene(url, null);
    }
    
    public void changerScene(String url, Callback<FXMLLoader, ?> callback) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(url));
        this.rootLayout.setCenter(loader.load());
        /* S'il y a un callback, on l'exécute */
        if(callback != null) callback.call(loader);
    }
    
    public void ouvrirModal(String url) throws IOException{
        ouvrirModal(url, null);
    }
    
    public boolean ouvrirModal(String url, Callback<FXMLLoader, ?> callback) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(url));
        AnchorPane page = (AnchorPane) loader.load();
        Scene scene = new Scene(page);
                
        /* S'il y a un callback, on l'exécute */
        if(callback != null) callback.call(loader);
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Modifier Usager");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        dialogStage.setScene(scene);
        dialogStage.showAndWait();
        
        ModalControleur controleur = loader.getController();
        return controleur.isOkCliked();
    }
}
