/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Nicolas
 */
public class NotificationService {

    //TODOSUP PUBLIC
    private static final NotificationService INSTANCE = new NotificationService();

    private final Properties mailServerProperties;

    private final Session mailSession;

    private final String mailMailServer;

    private final String passwordMailServer;

    private Properties properties;

    private static final ReservationService reservationService = ServiceFactory.getReservationService();

    private NotificationService() {
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        mailSession = Session.getDefaultInstance(mailServerProperties);

        try {
            initialiserProperty();
        } catch (IOException ex) {
            Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, "Problème à l'initialisation de la configuration du serveur mail", ex);
        }
        this.mailMailServer = properties.getProperty("mail");
        this.passwordMailServer = properties.getProperty("pwd");
    }

    private void initialiserProperty() throws IOException {
        try {
            properties = new Properties();
            File file = new File(getClass().getResource("/config/mail_server.properties").toURI());
            FileReader fr = new FileReader(file);
            try {
                properties.load(fr);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, "Fichier de configuration du serveur mail non trouvé", ex);
            } catch (IOException ex) {
                Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, "Problème à la configuration du serveur mail", ex);
            } finally {
                fr.close();
            }
        } catch (URISyntaxException ex) {
            Logger.getLogger(NotificationService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void notifier(Oeuvre oeuvre) throws MessagingException {
        List<Reservation> liste = reservationService.findAllByOeuvre(oeuvre);
        Transport transport = mailSession.getTransport("smtp");
        transport.connect("smtp.gmail.com", mailMailServer, passwordMailServer);
        MimeMessage message;
        String mail;
        for (Reservation reservation : liste) {
            mail = reservation.getUsager().getMail();
            if (!mail.equals("")) {
                message = new MimeMessage(mailSession);
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(mail));
                message.setSubject("Une oeuvre que vous avez réservée est disponible");
                message.setFrom(new InternetAddress(mailMailServer));
                String emailBody = "Bonjour, " + "<br><br> Vous avez réservé : "+oeuvre.infosOeuvre().getValue() + " cette oeuvre est maintenant disponible, <br><br>Bibliothécaire";
                message.setContent(emailBody, "text/html");
                transport.sendMessage(message, message.getAllRecipients());
                Logger.getLogger(NotificationService.class.getName()).log(Level.INFO, "Notification usager {0}", reservation.getUsager().personneProperty().getValue());
            }
        }
        transport.close();
    }

    public static NotificationService getInstance() {
        return INSTANCE;
    }
}
