/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.LivreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.MagazineService;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.OeuvreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.ReservationService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.UsagerService;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class ReserverOeuvreControleur implements Initializable {

    @FXML
    private TableView<Usager> tableUsager;
    @FXML
    private TableColumn<Usager, String> colonneUsager;
    @FXML
    private TextField filtreUsager;
    @FXML
    private TextField filtreOeuvre;
    @FXML
    private Button buttonFiltrerUsager;
    @FXML
    private Button buttonFiltrerOeuvre;
    @FXML
    private Button buttonReserver;
    @FXML
    private TableView<Livre> tableLivre;
    @FXML
    private TableColumn<Livre, String> colonneTitreLivre;
    @FXML
    private TableView<Magazine> tableMagazine;
    @FXML
    private TableColumn<Magazine, String> colonneTitreMagazine;
    @FXML
    private TableColumn<Magazine, String> colonneNumeroMagazine;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabLivre;
    @FXML
    private Tab tabMagazine;

    private static final UsagerService usagerService = ServiceFactory.getUsagerService();

    private static final LivreService livreService = ServiceFactory.getLivreService();

    private static final MagazineService magazineService = ServiceFactory.getMagazineService();

    private ObservableList<Usager> listeUsagers;

    private ObservableList<Livre> listeLivres;

    private ObservableList<Magazine> listeMagazines;

    private static final ReservationService reservationService = ServiceFactory.getReservationService();

    private static final OeuvreService oeuvreService = ServiceFactory.getOeuvreService();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colonneUsager.setCellValueFactory(cellData -> cellData.getValue().personneProperty());
        colonneNumeroMagazine.setCellValueFactory(cellData -> cellData.getValue().numeroProperty());
        colonneTitreLivre.setCellValueFactory(cellData -> cellData.getValue().titreProperty());
        colonneTitreMagazine.setCellValueFactory(cellData -> cellData.getValue().titreProperty());

        listeUsagers = FXCollections.observableList((List<Usager>) usagerService.findAll());
        listeLivres = FXCollections.observableList((List<Livre>) livreService.findAll());
        listeMagazines = FXCollections.observableList((List<Magazine>) magazineService.findAll());

        tableUsager.setItems(listeUsagers);
        tableLivre.setItems(listeLivres);
        tableMagazine.setItems(listeMagazines);
    }

    @FXML
    private void filtrerUsager(Event event) {
        String pattern = filtreUsager.getText();
        listeUsagers = FXCollections.observableList((List<Usager>) usagerService.findAllLike(pattern));
        tableUsager.setItems(null);
        tableUsager.setItems(listeUsagers);
    }

    @FXML
    private void filtrerOeuvre(Event event) {
        String pattern = filtreOeuvre.getText();
        listeLivres = FXCollections.observableList((List<Livre>) livreService.findAllLike(pattern));
        tableLivre.setItems(null);
        tableLivre.setItems(listeLivres);

        listeMagazines = FXCollections.observableList((List<Magazine>) magazineService.findAllLike(pattern));
        tableMagazine.setItems(null);
        tableMagazine.setItems(listeMagazines);
    }

    @FXML
    private void pressReserver() {
        Usager usager = tableUsager.getSelectionModel().getSelectedItem();
        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        Oeuvre oeuvre = (tab.equals(tabLivre)) ? tableLivre.getSelectionModel().getSelectedItem() : tableMagazine.getSelectionModel().getSelectedItem();
        if (usager != null && oeuvre != null) {
            Reservation reservation = new Reservation(usager, oeuvre);
            if (reservationService.exist(reservation)) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Erreur réservation");
                alert.setHeaderText(null);
                alert.setContentText("Cet usager possède déjà une réservation");
                alert.showAndWait();
            } else {
                reservationService.create(reservation);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Réservation réussie");
                alert.setHeaderText(null);
                alert.setContentText("La réservation de " + oeuvre.getTitre() + " par " + usager.getPrenom() + " " + usager.getNom() + " a été enregistré");
                alert.showAndWait();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur réservation");
            alert.setHeaderText(null);
            alert.setContentText("Un usager et une oeuvre doivent être renseignés");
            alert.showAndWait();
        }
    }
}
