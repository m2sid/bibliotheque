/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.BibliothequeApp;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Magazine;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.LivreService;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.MagazineService;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 *
 * @author Nicolas
 */
public class ListerOeuvreControleur implements Initializable {

    private static final LivreService livreService = ServiceFactory.getLivreService();

    private static final MagazineService magazineService = ServiceFactory.getMagazineService();

    private ObservableList<Livre> listeLivres;
    private ObservableList<Magazine> listeMagazines;

    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabPaneLivre;
    @FXML
    private Tab tabPaneMagazine;
    @FXML
    private TableView<Livre> tableLivre;
    @FXML
    private TableColumn<Livre, String> colonneLivreTitre;
    @FXML
    private TableColumn<Livre, String> colonneLivreAuteur;
    @FXML
    private TableColumn<Livre, String> colonneLivreResa;
    @FXML
    private TableColumn<Livre, String> colonneLivreResume;
    @FXML
    private TableView<Magazine> tableMagazine;
    @FXML
    private TableColumn<Magazine, String> colonneMagazineTitre;
    @FXML
    private TableColumn<Magazine, String> colonneMagazineNumero;
    @FXML
    private TableColumn<Magazine, String> colonneMagazineResa;
    @FXML
    private Button buttonReservation;
    @FXML
    private Button buttonExemplaire;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        colonneLivreTitre.setCellValueFactory(cellData -> cellData.getValue().titreProperty());
        colonneLivreAuteur.setCellValueFactory(cellData -> cellData.getValue().getAuteur().personneProperty());
        colonneLivreResa.setCellValueFactory(cellData -> cellData.getValue().nbResaProperty());
        colonneLivreResume.setCellValueFactory(cellData -> cellData.getValue().resumeProperty());

        listeLivres = FXCollections.observableList((List<Livre>) livreService.findAll());
        tableLivre.setItems(listeLivres);

        colonneMagazineTitre.setCellValueFactory(cellData -> cellData.getValue().titreProperty());
        colonneMagazineNumero.setCellValueFactory(cellData -> cellData.getValue().numeroProperty());
        colonneMagazineResa.setCellValueFactory(cellData -> cellData.getValue().nbResaProperty());

        listeMagazines = FXCollections.observableList((List<Magazine>) magazineService.findAll());
        tableMagazine.setItems(listeMagazines);

        tableLivre.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherButton());
        tableMagazine.getSelectionModel().selectedItemProperty().addListener(
                (observable, precedent, courant) -> afficherButton());

    }

    private void afficherButton() {
        buttonExemplaire.setDisable(false);
        buttonReservation.setDisable(false);
    }

    @FXML
    private void pressButtonReservation(ActionEvent event) throws IOException {
        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        Oeuvre oeuvre = (tab.equals(tabPaneLivre)) ? tableLivre.getSelectionModel().getSelectedItem() : tableMagazine.getSelectionModel().getSelectedItem();
        BibliothequeApp.getInstance().ouvrirModal("/fxml/ReservationsOeuvre.fxml", (loader) -> {
            ReservationsOeuvreControleur controleur = loader.getController();
            controleur.setOeuvre(oeuvre);
            return controleur;
        });
    }

    @FXML
    private void pressButtonExemplaire(ActionEvent event) throws IOException{
        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        Oeuvre oeuvre = (tab.equals(tabPaneLivre)) ? tableLivre.getSelectionModel().getSelectedItem() : tableMagazine.getSelectionModel().getSelectedItem();
        BibliothequeApp.getInstance().ouvrirModal("/fxml/EmpruntOeuvre.fxml", (loader) -> {
            EmpruntOeuvreControleur controleur = loader.getController();
            controleur.setOeuvre(oeuvre);
            return controleur;
        });
    }

}
