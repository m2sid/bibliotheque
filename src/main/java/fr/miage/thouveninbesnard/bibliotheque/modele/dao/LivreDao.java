/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Livre;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class LivreDao extends AbstractDao<Livre> {

    private static final LivreDao INSTANCE = new LivreDao();

    private LivreDao() {
    }

    public void create(Livre livre) {
        super.saveOrUpdate(livre);
    }

    public void delete(Livre livre) {
        super.delete(livre);
    }

    public List<Livre> findAll() {
        super.findAll(Livre.class);
        return super.findAll(Livre.class);
    }

    public int size() {
        return super.size(Livre.class);
    }

    public List<Livre> findAllLike(String pattern) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.like("titre", "%" + pattern + "%"));
        return super.findAll(Livre.class, criteres);
    }

    public List<Livre> findAllByAuteur(Auteur auteur) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("auteur", auteur));
        return super.findAll(Livre.class, criteres);
    }

    public boolean exist(Livre livre) {
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.eq("titre", livre.getTitre()));
        criteres.add(Restrictions.eq("auteur", livre.getAuteur()));
        return size(Livre.class, criteres) > 0;
    }

    public static LivreDao getInstance() {
        return LivreDao.INSTANCE;
    }
}
