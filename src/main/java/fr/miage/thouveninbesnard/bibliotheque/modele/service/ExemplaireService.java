/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.dao.ExemplaireDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Exemplaire;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class ExemplaireService implements Service<Exemplaire> {
    
    private static ExemplaireDao exemplaireDao;
    
    private static final ExemplaireService INSTANCE = new ExemplaireService();

    public ExemplaireService(){
        exemplaireDao = HibernateDaoFactory.getExemplaireDao();
    }
    
    @Override
    public void create(Exemplaire entity) {
        exemplaireDao.saveOrUpdate(entity);
    }

    @Override
    public void update(Exemplaire entity) {
         exemplaireDao.saveOrUpdate(entity);
    }

    @Override
    public void delete(Exemplaire entity) {
         exemplaireDao.delete(entity);
    }

    @Override
    public List<Exemplaire> findAll() {
         return exemplaireDao.findAll();
    }

    @Override
    public int size() {
        return exemplaireDao.size();
    }
    
    public int nbDispo(Oeuvre oeuvre) {
        return exemplaireDao.nbDispo(oeuvre);
    }     
    
    public List<Exemplaire> findAllByOeuvre(Oeuvre oeuvre){
        return exemplaireDao.findAllByOeuvre(oeuvre);
    }
    
    public int nbExemplaireOfOeuvre(Oeuvre oeuvre){
        return exemplaireDao.nbExemplaireOfOeuvre(oeuvre);
    }
    
    public Exemplaire getExemplaireDisponible(Oeuvre oeuvre){
        return exemplaireDao.getExemplaireDisponible(oeuvre);
    }
        
    public static ExemplaireService getInstance(){
        return ExemplaireService.INSTANCE;
    }
    
}
