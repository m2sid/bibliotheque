/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import javafx.util.StringConverter;

/**
 *
 * @author Nicolas
 */
public class PersonneStringConverter<E> extends StringConverter<Personne>{

    @Override
    public String toString(Personne entity) {
        return (entity == null)? null : entity.getNom()+" "+entity.getPrenom();
    }

    @Override
    public Personne fromString(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
