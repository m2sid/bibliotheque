/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.entite;

import fr.miage.thouveninbesnard.bibliotheque.util.DateUtil;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author Nicolas
 */
@Entity
@Table(name = "USAGER")
public class Usager extends Personne implements Serializable {

    private ObjectProperty<Date> dateNaissance = new SimpleObjectProperty();

    private StringProperty adresse = new SimpleStringProperty();

    private StringProperty mail = new SimpleStringProperty();

    private StringProperty telephone = new SimpleStringProperty();

    private List<Reservation> listeReservations;

    private List<Emprunt> listeEmprunts;

    public Usager(String nom, String prenom, LocalDate dateNaissance, String adresse, String mail, String telephone) {
        super(nom, prenom);
        if (dateNaissance != null) {
            this.dateNaissance.setValue(DateUtil.asDate(dateNaissance));
        }
        this.adresse.set(adresse);
        this.mail.setValue(mail);
        this.telephone.setValue(telephone);
    }

    public Usager() {
    }

    @Column(name = "dateNaissance")
    public Date getDateNaissance() {
        return dateNaissance.getValue();
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance.setValue(dateNaissance);
    }

    public ObjectProperty<LocalDate> dateNaissanceProperty() {
        return new SimpleObjectProperty<>(DateUtil.asLocalDate(dateNaissance.getValue()));
    }

    @Column(name = "adresse")
    public String getAdresse() {
        return adresse.get();
    }

    public void setAdresse(String adresse) {
        this.adresse.setValue(adresse);
    }

    public StringProperty adresseProperty() {
        return adresse;
    }

    @Column(name = "mail")
    public String getMail() {
        return mail.getValue();
    }

    public void setMail(String mail) {
        this.mail.setValue(mail);
    }

    public StringProperty mailProperty() {
        return mail;
    }

    @Column(name = "telephone")
    public String getTelephone() {
        return telephone.getValue();
    }

    public void setTelephone(String telephone) {
        this.telephone.setValue(telephone);
    }

    public StringProperty telephoneProperty() {
        return telephone;
    }

    @OneToMany(mappedBy = "usager")
    @Fetch(FetchMode.SELECT)
    @Cascade({CascadeType.MERGE, CascadeType.DELETE})
    public List<Reservation> getListeReservations() {
        return listeReservations;
    }

    public void setListeReservations(List<Reservation> listeReservations) {
        this.listeReservations = listeReservations;
    }

    @OneToMany(mappedBy = "usager")
    @Fetch(FetchMode.SELECT)
    @Cascade({CascadeType.MERGE, CascadeType.DELETE})
    public List<Emprunt> getListeEmprunts() {
        return listeEmprunts;
    }

    public void setListeEmprunts(List<Emprunt> listeEmprunts) {
        this.listeEmprunts = listeEmprunts;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usager other = (Usager) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.dateNaissance, other.dateNaissance)) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.telephone, other.telephone)) {
            return false;
        }
        return true;
    }

    public void updateModele(Usager temp) {
        nom = temp.nom;
        prenom = temp.prenom;
        dateNaissance = temp.dateNaissance;
        adresse = temp.adresse;
        mail = temp.mail;
        telephone = temp.telephone;
    }

    @Override
    public String toString() {
        return "Usager{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + ", adresse=" + adresse + ", mail=" + mail + ", telephone=" + telephone + '}';
    }
}
