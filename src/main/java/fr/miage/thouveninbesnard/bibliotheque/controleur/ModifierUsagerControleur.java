/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.UsagerService;
import fr.miage.thouveninbesnard.bibliotheque.util.DateUtil;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class ModifierUsagerControleur extends ModalControleur implements Initializable {
    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private DatePicker dateNaissance;
    @FXML
    private TextField adresse;
    @FXML
    private TextField mail;
    @FXML
    private TextField telephone;
    @FXML
    private Button enregistrer;
    @FXML
    private Button annuler;
    
    private Usager usager;
    
    private static final UsagerService usagerService = ServiceFactory.getUsagerService();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    @FXML
    private void pressEnregistrer(ActionEvent event) throws IOException{
        okCliked = true;
        Usager usagerTemp = new Usager(nom.getText(), prenom.getText(), dateNaissance.getValue(), adresse.getText(), mail.getText(), telephone.getText());
        if(!usager.equals(usagerTemp)){
            this.usager.updateModele(usagerTemp);
        }
        usagerService.update(usager);
        closeModal(event);
    }
    
    @FXML
    private void pressAnnuler(ActionEvent event) throws IOException{
        closeModal(event);
    }
    
    @FXML
    public void setUsager(Usager usager) {
        this.usager = usager;
        nom.setText(this.usager.getNom());
        prenom.setText(this.usager.getPrenom());
        Date date = this.usager.getDateNaissance();
        if(date != null) dateNaissance.setValue(DateUtil.asLocalDate(date));
        adresse.setText(this.usager.getAdresse());
        mail.setText(this.usager.getMail());
        telephone.setText(this.usager.getTelephone());
    }
}
