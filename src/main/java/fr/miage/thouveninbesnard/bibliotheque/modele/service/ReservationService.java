/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.service;

import fr.miage.thouveninbesnard.bibliotheque.modele.dao.ReservationDao;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Oeuvre;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Reservation;
import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.HibernateDaoFactory;
import java.util.List;

/**
 *
 * @author Nicolas
 */
public class ReservationService implements Service<Reservation>{
    
    public static ReservationDao reservationDao;
    
    private static final ReservationService INSTANCE = new ReservationService();
    
    private ReservationService(){
        reservationDao = HibernateDaoFactory.getReservationDao();
    }
    
    @Override
    public void create(Reservation reservation){
        reservationDao.saveOrUpdate(reservation);
    }
    
    @Override
    public void update(Reservation reservation) {
        reservationDao.saveOrUpdate(reservation);
    }
    
    @Override
    public void delete(Reservation reservation){
        reservationDao.delete(reservation);
    }
    
    @Override
    public List<Reservation> findAll(){
        return reservationDao.findAll();
    }

    @Override
    public int size() {
     return reservationDao.size();
    }
    
    public int getNbResaOfOeuvre(Oeuvre oeuvre) {
        return reservationDao.getNbResaOfOeuvre(oeuvre);
    }
    
    public List<Reservation> findAllByUsager(Usager usager){
        return reservationDao.findAllByUsager(usager);
    }
    
    public List<Reservation> findAllByOeuvre(Oeuvre oeuvre){
        return reservationDao.findAllByOeuvre(oeuvre);
    }
    
    public boolean usagerHasReserveOeuvre(Usager usager, Oeuvre oeuvre){
        return reservationDao.usagerHasReserveOeuvre(usager, oeuvre);
    }
    
    public Reservation findOneBy(Usager usager, Oeuvre oeuvre){
        return reservationDao.findOneBy(usager, oeuvre);
    }
    
    public boolean exist(Reservation reservation){
        return reservationDao.exist(reservation);
    }
    
    public static ReservationService getInstance(){
        return ReservationService.INSTANCE;
    }
}
