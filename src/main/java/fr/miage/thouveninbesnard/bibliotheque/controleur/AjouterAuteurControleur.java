/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.controleur;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Auteur;
import fr.miage.thouveninbesnard.bibliotheque.modele.service.AuteurService;
import fr.miage.thouveninbesnard.bibliotheque.modele.factory.ServiceFactory;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Nicolas
 */
public class AjouterAuteurControleur implements Initializable {

    @FXML
    private TextField nom;
    @FXML
    private TextField prenom;
    @FXML
    private Button enregistrer;

    private final AuteurService auteurService = ServiceFactory.getAuteurService();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void pressEnregistrer(ActionEvent event) {
        Auteur nouveauAuteur = new Auteur(nom.getText(), prenom.getText());
        if (auteurService.exist(nouveauAuteur)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(nouveauAuteur.getNom() + " " + nouveauAuteur.getNom() + " existe déjà");
            alert.showAndWait();
        }
        else{
            auteurService.create(nouveauAuteur);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(null);
            alert.setHeaderText(null);
            alert.setContentText(nouveauAuteur.getNom() + " " + nouveauAuteur.getNom() + " enregistré");
            alert.showAndWait();
        }
    }
}
