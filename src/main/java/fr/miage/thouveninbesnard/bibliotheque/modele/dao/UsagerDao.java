/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.miage.thouveninbesnard.bibliotheque.modele.dao;

import fr.miage.thouveninbesnard.bibliotheque.modele.entite.Usager;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Nicolas
 */
public class UsagerDao extends AbstractDao<Usager> {
    
    private static final UsagerDao INSTANCE = new UsagerDao();
    
    private UsagerDao(){
        super();
    }
    
    public void create(Usager usager){
        super.saveOrUpdate(usager);
    }
    
    public void delete(Usager usager){
        super.delete(usager);
    }
    
    public List<Usager> findAll(){
        return super.findAll(Usager.class);
    }

    public List<Usager> findAllLike(String pattern){        
        List<Criterion> criteres = new LinkedList();
        criteres.add(Restrictions.or(Restrictions.like("nom", "%"+pattern+"%"), Restrictions.like("prenom", "%"+pattern+"%")));
        return super.findAll(Usager.class, criteres);
    }
    
    public int size() {
       return super.size(Usager.class);
    }
    
    public static UsagerDao getInstance(){
        return UsagerDao.INSTANCE;
    }
}

